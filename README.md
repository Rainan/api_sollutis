# API - Solluti Tecnologias
## Desenvolvimento de API para Vaga de BackEnd

Este projeto faz parte do processo seletivo para Vaga de BackEnd,
tendo como objetivo o desenvolvimento um API utilizando o Framework Laravel. 
Os requisitos (tanto obrigatórios como opcionais) para essa API, seguem abaixo:

## Estrutura do Projeto

- **Arquitetura recomendada:** Repository.
- **Testes:** Testes unitários e integrados valem mais pontos.
- **Frontend:** Não é necessária a sua construção, mas caso seja feita será considerado com
  mais pontos dependendo da qualidade do código.
- **Best Practices:** Serão consideradas como diferencial e dão mais pontos se forem observados.

## Regras
- **Email:** Ao criar ou atualizar um produto ele deve armazenar em um banco de dados e gerar uma 
notificação de sucesso através de email.
- **Mutator:** Ao retornar o campo de valor nas Apis através de um mutator criar uma máscara de
R$ ####,##.
 - **CRUD:** Os CRUDs tanto de LOJA quanto de PRODUTO dever ser completo com todas as rotas para API: index, show, store,
update, delete.
- **Visualização:** Ao visualizar uma loja já devemos carregar todos os produtos dessa loja.

## Estrutura dos dados
Aqui é mostrado como será a estrutura das tabelas no banco de dados e as suas regras

-  **Produto**

    - Nome
        - String
        - Máx 60 caracteres
        - Min 3 caracteres
        - Mensagem de erros para cada validação em português

    - Valor
        - Integer
        - Mín de 2 caracteres
        - Máx de 6 caracteres
        - mensagem de erros para cada validação em português

    - Loja_id
        - Integer

    - Ativo
        - Bollean

-  **Loja**

    - Nome da Loja
        - String
        - Máx 40 caracteres
        - Min 3 caracteres
        - Mensagem de erros para cada validação em português

    - Email
        - String
        - Deve ser do tipo email
        - Deve ser único
        - Mensagem de erros para cada validação em português
        
## .ENV

Para configurar o envio de email, pode ser utilizado as configurações do email teste informado abaixo, 
ou adicionar algum outro pelo arquivo .ENV do projeto

```sh
MAIL_MAILER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=587
MAIL_ENCRYPTION=tls
MAIL_USERNAME=testelaravelemail123@gmail.com
MAIL_PASSWORD=abcde!@3456
MAIL_FROM_ADDRESS=testelaravelemail123@gmail.com
```

## Contato

Me chamo Rainan Gramacho, segue abaixo minhas redes sociais e email para contato:

| Tipo Contato | Endereço |
| ------ | ------ |
| Linkedin | https://www.linkedin.com/in/rainan-gramacho-568527137/|
| GitHub | https://github.com/Rainangramacho |
| Email  | rainan.gramacho@hotmail.com |




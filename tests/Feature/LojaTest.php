<?php

namespace Tests\Feature;

use App\Models\Loja;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LojaTest extends TestCase
{
    /** @test */
    public function checa_se_tabela_loja_esta_correta()
    {
        $loja = new Loja();
        $dados_esperados = [
            'nome',
            'email',
        ];

        $comparacao = array_diff($dados_esperados, $loja->getFillable());
        $this->assertEquals(0, count($comparacao));
    }

     /** @test */
     public function checa_se_existem_emails_duplicados()
     {
         $loja = Loja::select('email')->get()->toArray();
         $count = count(array_map("unserialize", array_unique(array_map("serialize", $loja))));
         $this->assertTrue($count == count($loja));
     }
}

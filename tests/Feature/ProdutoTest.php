<?php

namespace Tests\Feature;

use App\Models\Loja;
use App\Models\Produto;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProdutoTest extends TestCase
{
    /** @test */
    public function checa_se_tabela_produto_esta_correto()
    {
        $produto = new Produto();
        $dados_esperados = [
            'nome',
            'valor',
            'loja_id',
            'ativo'
        ];

        $comparacao = array_diff($dados_esperados, $produto->getFillable());
        $this->assertEquals(0, count($comparacao));
    }

    /** @test */
    public function checa_se_loja_cadastrada_em_produto_existe()
    {
        $produto = Produto::selectRaw('distinct(loja_id)')->orderBy('loja_id')->get()->toArray();
        $loja = Loja::whereIn('id', $produto)->count();

        $this->assertEquals(true, $loja > 0);
    }
}

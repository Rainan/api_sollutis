<?php

namespace App\Rules;

use App\Repository\LojaRepository;
use Illuminate\Contracts\Validation\Rule;

class EmailValidacaoRule implements Rule
{
    private $lojaId;
    /**
     * Método construtor
     *
     * @return void
     */
    public function __construct($lojaId = null)
    {
        $this->lojaId = $lojaId;
    }

    /**
     * Determina se a validação passou
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(!$this->validaIdRota($this->lojaId))
            return false;

        $loja = new LojaRepository();
        $existe = $loja->buscarPorEmail($value, $this->lojaId) > 0;
        return !$existe;
    }

    /**
     * Gera a menssagem de erro da validação
     *
     * @return string
     */
    public function message()
    {
        if(!$this->validaIdRota($this->lojaId))
            return 'Informe o id da loja corretamente!';

        return 'O email informado já está cadastrado!';
    }

     /**
     * Valida se o id informado é um valor válido
     *
     * @return string
     */
    public function validaIdRota($lojaId){
        $retorno = true;
        if(!empty($lojaId) && !is_numeric($lojaId))
            $retorno = false;

        return $retorno;
    }
}

<?php

namespace App\Rules;

use App\Repository\LojaRepository;
use Illuminate\Contracts\Validation\Rule;

class LojaExisteRule implements Rule
{
    /**
     * Método construtor
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determina se a validação passou
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $loja = new LojaRepository();
        if($loja->listarPeloId($value) == null)
            return false;

        return true;
    }

    /**
     * Gera a menssagem de erro da validação
     *
     * @return string
     */
    public function message()
    {
        return 'A loja informada não existe!';
    }
}

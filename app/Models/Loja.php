<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Loja extends Model
{
    use Notifiable;

    protected $table = 'loja';
    protected $fillable = ['nome', 'email'];

    /**
     * Função que retorna o relacionamento de loja com produto
     *
     * @return void
     */
    public function produto()
    {
        return $this->hasMany(Produto::class, 'loja_id', 'id');
    }
}

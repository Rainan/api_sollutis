<?php

namespace App\Models;

use App\Helpers\FormataValores;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Produto extends Model
{
    use Notifiable;

    protected $table = 'produto';
    protected $fillable = ['nome', 'valor', 'loja_id', 'ativo'];

    /**
     * Mutator criado para adicionar máscara no campo valor que for retornado
     *
     * @param  string  $value
     * @return string
     */
    public function getValorAttribute($value)
    {
        return 'R$ ' . FormataValores::dinheiro($value);
    }

    /**
     * Função que retorna o relacionamento de produto e loja
     *
     * @return void
     */
    public function loja(){
        return $this->belongsTo(Loja::class, 'id', 'loja_id');
    }
}

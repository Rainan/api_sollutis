<?php

namespace App\Http\Requests;

use App\Rules\EmailValidacaoRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class LojaAtualizaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => [
                'string',
                'min:3',
                'max:40',
            ],
            'email' => [
                'email',
                new EmailValidacaoRule(Route::current()->parameters()['loja'])
            ],
        ];
    }
}

<?php

namespace App\Http\Requests;

use App\Rules\EmailValidacaoRule;
use App\Rules\LojaExisteRule;
use Illuminate\Foundation\Http\FormRequest;

class ProdutoCadastroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => [
                'string',
                'required',
                'min:3',
                'max:60'
            ],
            'valor' => [
                'required',
                'numeric',
                'digits_between:2,6'
            ],
            'loja_id' => [
                'required',
                'numeric',
                new LojaExisteRule()
            ],
            'ativo' => [
                'required',
                'boolean'
            ]
        ];
    }
}

<?php

namespace App\Http\Controllers;

use App\Helpers\Excecao;
use App\Http\Requests\LojaAtualizaRequest;
use App\Http\Requests\LojaCadastroRequest;
use App\Repository\LojaRepository;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Exception;

// @todo - mudar nome do database no .env
class LojaController extends Controller
{
    private $loja;

    public function __construct()
    {
        $this->loja = new LojaRepository();
    }

    /**
     * Lista as lojas cadastrasdas
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->loja->listarTodos(), Response::HTTP_OK);
    }

    /**
     * Cadastra os dados de uma loja
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LojaCadastroRequest $request)
    {
        try {
            $salvar = $this->loja->salvar($request->all());

            if($salvar instanceof Exception)
                throw new Exception($salvar);

            return response()->json(
                [
                    'retorno' => true,
                    'mensagem' => 'Loja registrada com sucesso!',
                    'loja' => $salvar
                ],Response::HTTP_OK);

        } catch (\Exception $e) {
            return Excecao::montar('Não foi possível salvar a loja!', $e);
        }
    }

    /**
     * Lista os dados de uma loja específica
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json($this->loja->listarPeloId($id), Response::HTTP_OK);
    }

    /**
     * Atualiza os dados de uma loja
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LojaAtualizaRequest $request, $id)
    {
        try {
            $atualizar = $this->loja->atualizar($request->all(), $id);

            if($atualizar instanceof Exception)
                throw $atualizar;

            return response()->json(
                [
                    'retorno' => true,
                    'mensagem' => 'Loja atualizada com sucesso!',
                    'loja' => $atualizar
                ], Response::HTTP_OK);

        } catch (\Exception $e) {
                return Excecao::montar('Não foi possível atualizar a loja', $e);
        }
    }

    /**
     * Remove os dados de uma loja do banco de dados
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $deletar = $this->loja->remover($id);

            if ($deletar instanceof Exception)
                throw $deletar;

            return response()->json(['retorno' => true, 'mensagem' => 'Loja removida com sucesso!'], Response::HTTP_OK);
        } catch (\Exception $e) {
            return Excecao::montar('Não foi possível remover a loja!', $e);
        }
    }
}

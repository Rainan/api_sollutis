<?php

namespace App\Http\Controllers;

use App\Helpers\Excecao;
use App\Http\Requests\ProdutoAtualizaRequest;
use App\Http\Requests\ProdutoCadastroRequest;
use App\Repository\ProdutoRepository;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Exception;

class ProdutoController extends Controller
{
    private $produto;

    public function __construct()
    {
        $this->produto = new ProdutoRepository();
    }
    /**
     * Lista os produtos cadastrasdos
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->produto->listarTodos(), Response::HTTP_OK);
    }

    /**
     *  Cadastra os dados do produto
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProdutoCadastroRequest $request)
    {
        try {
            $salvar = $this->produto->salvar($request->all());

            if($salvar instanceof Exception)
                throw new Exception($salvar);

            return response()->json(
                [
                    'retorno' => true,
                    'mensagem' => 'Produto registrado com sucesso!',
                    'produto' => $salvar
                ],Response::HTTP_OK);

        } catch (\Exception $e) {
            return Excecao::montar('Não foi possível salvar o produto!', $e);
        }
    }

    /**
     * Lista os dados de um produto específico
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json($this->produto->listarPeloId($id), Response::HTTP_OK);
    }

    /**
     *  Atualiza os dados de um produto
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProdutoAtualizaRequest $request, $id)
    {
        try {
            $atualizar = $this->produto->atualizar($request->all(), $id);

            if($atualizar instanceof Exception)
                throw $atualizar;

            return response()->json(
                [
                    'retorno' => true,
                    'mensagem' => 'Produto atualizado com sucesso!',
                    'produto' => $atualizar
                ], Response::HTTP_OK);

        } catch (\Exception $e) {
                return Excecao::montar('Não foi possível atualizar o produto', $e);
        }
    }

    /**
     * Remove os dados de uma loja do banco de dados
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $deletar = $this->produto->remover($id);

            if ($deletar instanceof Exception)
                throw $deletar;

            return response()->json(['retorno' => true, 'mensagem' => 'Produto removido com sucesso!'], Response::HTTP_OK);
        } catch (\Exception $e) {
            return Excecao::montar('Não foi possível remover o produto!', $e);
        }
    }

}

<?php

namespace App\Notifications;

use App\Models\Produto;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NotifyLoja extends Notification
{
    use Queueable;

    private $details;
    private $acao;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($details, $acao)
    {
        $this->details = $details;
        $this->acao = $acao;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->subject($this->acao)
        ->greeting($this->details['saudacao'])
        ->line($this->details['cabecalho'])
        ->line($this->details['nome'])
        ->line($this->details['valor'])
        ->line($this->details['loja']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

<?php

namespace App\Repository;
use App\Models\Loja;

class LojaRepository{

    private $loja;

    public function __construct()
    {
        $this->loja = new Loja();
    }

    /**
     * Função que lista todas as lojas
     *
     * @return void
     */
    public function listarTodos()
    {
        return $this->loja->all();
    }

    /**
     * Função que retorna uma loja pelo id
     *
     * @param [type] $id
     * @return void
     */
    public function listarPeloId(int $id)
    {
        return $this->loja::with('produto')->find($id);
    }

    /**
     * Função que salva uma loja
     *
     * @param Request $request
     * @return void
     */
    public function salvar(array $dados)
    {
        try {
           return $this->loja::create($dados);
        } catch (\Exception $e) {
           return $e;
        }
    }

     /**
     * Função que atualiza os dados de uma loja
     *
     * @param LojaAtualizarRequest $request
     * @param [type] $id
     * @return void
     */
    public function atualizar(array $dados, $id)
    {
        try {
            $this->loja::find($id)->update($dados);
            return $this->loja->find($id);
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Função que remove a loja do banco de dados
     * @param [type] $id
     * @return void
     */
    public function remover(int $id){

        try {
            return  $this->loja->find($id)->delete();
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Função que verifica se o email informado já está cadastrado, e se já estiver, se ele pertence a empresa informada
     * @param string $email
     * @return void
     */
    public function buscarPorEmail(string $email, $lojaId = null)
    {
        $dados = $this->loja::where(['email' => $email]);

        if(!empty($lojaId))
            $dados = $dados->where('id', '<>', $lojaId);

        return $dados->count();
    }

}

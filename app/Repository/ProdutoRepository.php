<?php

namespace App\Repository;

use App\Helpers\EnviarEmail;
use App\Models\Loja;
use App\Models\Produto;
use App\Notifications\NotifyLoja;
use Exception;

class ProdutoRepository{

    private $produto;

    public function __construct()
    {
        $this->produto = new Produto();
    }

    /**
     * Função que lista todos os produtos
     *
     * @return void
     */
    public function listarTodos()
    {
        return $this->produto->all();
    }

    /**
     * Função que retorna um produto pelo id
     *
     * @param [type] $id
     * @return void
     */
    public function listarPeloId(int $id)
    {
        return $this->produto::find($id);
    }

    /**
     * Função que salva um produto
     *
     * @param Request $request
     * @return void
     */
    public function salvar(array $dados)
    {
        try {
            $this->produto::create($dados);
            $produto = $this->produto->orderBy('ID', 'DESC')->first();
            $loja = Loja::find($produto->loja_id, ['email', 'nome']);

            $details = [
                'saudacao' => "Olá, $loja->nome!",
                'cabecalho' => 'Um novo produto foi cadastrado! Segue abaixo as informações:',
                'nome' => "Produto: $produto->nome",
                'valor' => "Valor: $produto->valor",
                'loja' => "Loja: $loja->nome",
            ];

            $loja->notify(new NotifyLoja($details, 'Cadastro de Produto'));
            return $produto;

        } catch (\Exception $e) {
            return $e;
        }
    }

     /**
     * Função que atualiza os dados de um produto
     *
     * @param ProdutoAtualizarRequest $request
     * @param [type] $id
     * @return void
     */
    public function atualizar(array $dados, $id)
    {
        try {
            $this->produto::find($id)->update($dados);
            $produto =  $this->produto->find($id);
            $loja = Loja::find($produto->loja_id, ['email', 'nome']);

            $details = [
                'saudacao' => "Olá, $loja->nome!",
                'cabecalho' => 'Os dados de um de seus produtos cadastrados foram alterados! Segue informações abaixo:',
                'nome' => "Produto: $produto->nome",
                'valor' => "Valor: $produto->valor",
                'loja' => "Loja: $loja->nome",
                'thanks' => 'Thank you',
            ];

            $loja->notify(new NotifyLoja($details, 'Atualização do Produto'));
            return $produto;
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Função que remove o produto do banco de dados
     * @param [type] $id
     * @return void
     */
    public function remover(int $id){

        try {
            return  $this->produto->find($id)->delete();
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Função que monta o corpo do email para envio
     * @param [Object] $produto
     * @return void
     */
    public function montaCorpoEmail($produto, $loja, $assunto, $mensagem){
        return array('assunto' => $assunto, 'produto' => $produto, 'loja' => $loja, 'mensagem' => $mensagem);
    }

}

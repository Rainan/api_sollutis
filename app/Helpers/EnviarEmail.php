<?php

namespace App\Helpers;

use App\Mail\ConfigEmail;
use Mail;
class EnviarEmail{

    public static function enviar($params, $email)
    {
        try {
            Mail::to($email)->send(new ConfigEmail($params));
            return true;
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}

<?php

namespace App\Helpers;

class FormataValores{

    /**
     * Função que converte um numero no formato de dinheiro em Float
     * para ser salvo no banco de dados
     *
     * @param [type] $string
     * @return float
     */
    public static function strToFloat($string): float
    {
        $string = explode(',', $string);
        $parte1 = str_replace('.', '', $string[0]);
        $parte2 = self::verifyKeyArray(1, $string) ? $string[1] : 0;
        $numero_en_format = $parte1.'.'.$parte2;
        return floatval($numero_en_format);
    }

    /**
     * Função que retorna um valor em float em formato de dinheiro
     *
     * @param [type] $float
     * @param integer $decimais
     * @return void
     */
    public static function dinheiro($float, $decimais=2)
    {
        return ($float == "" || $float == null) ? "" : number_format($float, $decimais, ',', '.' );
    }

}

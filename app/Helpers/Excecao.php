<?php

namespace App\Helpers;

use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use InvalidArgumentException;

class Excecao{

    /**
     * @param string $mensagem
     * @param Exception $e
     * @return JsonResponse
     * @throws InvalidArgumentException
     * @throws BindingResolutionException
     */
    public static function montar(string $mensagem, Exception $e): JsonResponse
    {
        $contentError = [
            'retorno' => false,
            'mensagem' =>  (preg_match('/Erro/', $e->getMessage()) >= 1 ? $e->getMessage() : $mensagem)
        ];

        if(env('APP_DEBUG')){
            $contentError['sysError'] =
            'Mensagem: ' . $e->getMessage() .
            ' | Arquivo: ' . $e->getFile() .
            ' | Linha: ' . $e->getLine() .
            ' | Codigo: ' . $e->getCode();
        }

        return response()->json($contentError, Response::HTTP_INTERNAL_SERVER_ERROR);
    }


}

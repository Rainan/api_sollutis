<?php

namespace App\Exceptions;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

use function PHPUnit\Framework\isInstanceOf;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (Throwable $e, $request) {
            $contentError = [
                'retorno' => false,
                'mensagem' => 'Houve uma falha ao realizar sua solicitação!'
            ];

            if($e instanceof ValidationException){
                $contentError['mensagensValidacao'] = $e->validator->getMessageBag()->messages();
            }

            if(env('APP_DEBUG')){
                $contentError['catchMsg'] = 'Mensagem: ' . $e->getMessage() . ' | Arquivo: ' . $e->getFile() . ' | Linha: ' . $e->getLine() . ' | Codigo: ' . $e->getCode();
            }

            return response()->json($contentError, Response::HTTP_INTERNAL_SERVER_ERROR);
        });
    }

}

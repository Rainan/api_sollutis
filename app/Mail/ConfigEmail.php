<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Helpers\FormataValores;

class ConfigEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $params;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($params)
    {
        $this->params = $params;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.email',[
            'params' => $this->params,
            'formataValor' => new FormataValores()
        ])->subject($this->params['assunto'] . ' - '. 'Loja ' . $this->params['loja']->nome);
    }
}

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>API - Sollutis</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="text-center col-md-12" style="margin-top: 22%">
                <img class="mt-12 " src="/images/siacondLogo.jpeg" alt="" srcset="">
            </div>
            <div class="text-center col-md-12">
                <p><b>API Rest em Desenvolvimento!</b></p>
            </div>
        </div>
    </div>
</body>
</html>
